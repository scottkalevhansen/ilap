'''
ilap_test.py

Tests the implementations of the Fixed Talbot scheme by solving for the breakthrough
curve resulting from solution of the advection-dispersion equation with a Dirac source
at the origin. The analytic (inverse Gaussian) solution is compared with inversion of
the Laplace transform with fixed precision invert() and arbitrary precision invert_mp()
'''
from matplotlib.pyplot import plot, legend, show, title, xlabel, ylabel
from ilap.ilap_utils import invert, invert_mp
import mpmath as mpm
import numpy as np
from numpy import fromiter, float64, linspace, vectorize

def L(alpha, d, v):
    return d**2/(2*alpha*v)

def M(d, v):
    return d/v 

def phi_trans(s, d, alpha, v):
    def ig_trans(s, L, M):
        return np.exp(L/M - np.sqrt(L/M**2+ 2*s)/np.sqrt(1/L))
    return ig_trans(s, L(alpha, d, v), M(d, v))

def phi_trans_mp(s, d, alpha, v):
    def ig_trans(s, L, M):
        return mpm.exp(L/M - mpm.sqrt(L/M**2+ 2*s)/mpm.sqrt(1/L))
    return ig_trans(s, L(alpha, d, v), M(d, v))

def phi_direct(t, d, alpha, v):
    def ig_direct(t, L, M):
        return np.sqrt(L/(2*np.pi*t**3))*np.exp(-L*(t-M)**2/(2*M**2*t))
    return ig_direct(t, L(alpha, d, v), M(d, v))

d = 1.5 #m
alpha = 3e-2 #m

v = 0.53e-3
v_t = linspace(1, 7000, num=100)
df = vectorize(lambda t: phi_direct(t, d, alpha, v))
v_PDF = df(v_t)
plot(v_t, v_PDF, 'ks', markerfacecolor='none', ms=6, markeredgecolor='darkolivegreen', label="Exact solution")

lt = lambda s: phi_trans(s, d, alpha, v)
v_PDFt = fromiter(map(lambda t: invert(lt,t), v_t), dtype=float64)
plot(v_t, v_PDFt, lw=2, color="navy", label="Solution via invert()")

lt = lambda s: phi_trans_mp(s, d, alpha, v)
v_PDFt = fromiter(map(lambda t: invert_mp(lt,t), v_t), dtype=float64)
plot(v_t, v_PDFt, ls=(0, (3, 3)), lw=2, color="goldenrod", label="Solution via invert_mp()")
xlabel("Time")
ylabel("Probability density")
title("Test of ilap module inversion methods on ADE solution")
legend()
show()